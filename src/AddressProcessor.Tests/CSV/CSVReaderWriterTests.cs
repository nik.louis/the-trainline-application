﻿using System;
using AddressProcessing.CSV;
using NUnit.Framework;

namespace AddressProcessing.Tests.CSV
{
    [TestFixture]
    public class CsvReaderWriterTests
    {
        private CSVReaderWriter _sut;
        private const string FileName = @"test_data\contacts.csv";

        [SetUp]
        public void Setup()
        {
            _sut = new CSVReaderWriter();
        }

        [TearDown]
        public void TearDown()
        {
            _sut.Close();
        }

        [Test]
        [ExpectedException(typeof(Exception), ExpectedMessage = "The file 'non existing file' doesn't exist")]
        public void Open_NonExistingFile_ThrowsException() //Roy Osherove's 'The Art Of Unit Testing' tests naming convention
        {
            _sut.Open("non existing file", CSVReaderWriter.Mode.Read);
        }

        [Test]
        [ExpectedException(typeof(Exception), ExpectedMessage = "Unknown file mode for " + FileName)]
        public void Open_UnknownMode_ThrowsException()
        {
            _sut.Open(FileName, (CSVReaderWriter.Mode)33);
        }

        [Test]
        [ExpectedException(typeof(Exception), ExpectedMessage = "A file in Write mode must be opened before writing")]
        public void Write_StreamNotOpenForWriting_ThrowsException()
        {
            _sut.Write("my", "strings");
        }

        [Test]
        [ExpectedException(typeof(Exception), ExpectedMessage = "A file in Read mode must be opened before reading")]
        public void Write_StreamNotOpenForReading_ThrowsException()
        {
            _sut.Read("my", "strings");
        }

        [Test]
        public void ByValRead_FileOpenForRead_ReachesTheEndOfTheFileWithoutExceptions()
        {
            _sut.Open(FileName, CSVReaderWriter.Mode.Read);
            while (_sut.Read("pointless", "param"))
            {

            }
        }

        [Test]
        public void Read_FileOpenForRead_ReadsActualFileValues()
        {
            string column1;
            string column2;

            _sut.Open(FileName, CSVReaderWriter.Mode.Read);

            var actual = _sut.Read(out column1, out column2);
            
            Assert.IsTrue(actual);
            Assert.AreEqual(column1, "Shelby Macias");
            Assert.AreEqual(column2, "3027 Lorem St.|Kokomo|Hertfordshire|L9T 3D5|England");
        }
    }
}
