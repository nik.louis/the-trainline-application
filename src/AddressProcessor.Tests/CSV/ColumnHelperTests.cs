﻿using System.Runtime.InteropServices;
using AddressProcessing.CSV;
using NUnit.Framework;

namespace AddressProcessing.Tests.CSV
{
    [TestFixture]
    public class ColumnHelperTests
    {
        private readonly char[] _separator = {'\t'};
        private IColumnHelper _sut;

        [SetUp]
        public void Setup()
        {
            _sut = new ColumnHelper(_separator);
        }

        [Test]
        public void BuildLineFromColumns_ThreeStrings_ReturnsSeparatedLine()
        {
            const string expected = "string1\tstring2\tstring3";
            var actual = _sut.BuildLineFromColumns("string1", "string2", "string3");

            Assert.AreEqual(expected,actual);
        }

        [Test]
        public void ReadCsvLine_ValidCsvLine_ReturnsTrueAndColumnValues()
        {
            const string input = "string1\tstring2\tstring3";
            var actual = _sut.ReadCsvLine(input);

            Assert.IsTrue(actual.HasValues);
            Assert.AreEqual("string1", actual.Column1);
            Assert.AreEqual("string2", actual.Column2);
        }

        [Test]
        public void ReadCsvLine_NullCsvLine_ReturnsFalseAndNullColumnValues()
        {
            const string input = null;
            var actual = _sut.ReadCsvLine(input);

            Assert.IsFalse(actual.HasValues);
            Assert.IsNull(actual.Column1);
            Assert.IsNull(actual.Column2);
        }

        [Test]
        public void ReadCsvLine_SingeValueCsvLine_ReturnsTrueAndOnlyOneValue()
        {
            const string input = "string1";
            var actual = _sut.ReadCsvLine(input);

            Assert.IsTrue(actual.HasValues);
            Assert.AreEqual("string1", actual.Column1);
            Assert.IsNull(actual.Column2);
        }
    }
}
