﻿using System;
using AddressProcessing.CSV;
using NUnit.Framework;

namespace AddressProcessing.Tests.CSV
{
    class CsvReaderWriterForAnnotationsTests
    {
        private CSVReaderWriterForAnnotation _sut;
        private const string FileName = @"test_data\contacts.csv";

        [SetUp]
        public void Setup()
        {
            _sut = new CSVReaderWriterForAnnotation();
        }

        [TearDown]
        public void TearDown()
        {
            _sut.Close();
        }

        [Test]
        [ExpectedException(typeof(Exception), ExpectedMessage = "Unknown file mode for " + FileName)]
        public void Open_ReadAndWriteTogether_ThrowsException() //Roy Osherove's 'The Art Of Unit Testing' tests naming convention
        {
            _sut.Open(FileName, CSVReaderWriterForAnnotation.Mode.Read | CSVReaderWriterForAnnotation.Mode.Write);
        }

        [Test]
        [ExpectedException(typeof(NullReferenceException))]
        public void Read_ValidFileAndWorkflow_ThrowsException()
        {
            _sut.Open(FileName, CSVReaderWriterForAnnotation.Mode.Read);
            while (_sut.Read("", ""))
            {

            }
        }
    }
}
