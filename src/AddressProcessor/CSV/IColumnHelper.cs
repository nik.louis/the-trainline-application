﻿using System.Linq.Expressions;
using System.Security.Cryptography.X509Certificates;
using AddressProcessing.Model;

namespace AddressProcessing.CSV
{
    public interface IColumnHelper
    {
        string BuildLineFromColumns(params string[] columns);

        LineResult ReadCsvLine (string csvLine);
    }
}
