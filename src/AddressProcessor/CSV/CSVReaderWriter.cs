﻿using System;
using System.IO;

namespace AddressProcessing.CSV
{
    /*
        2) Refactor this class into clean, elegant, rock-solid & well performing code, without over-engineering.
           Assume this code is in production and backwards compatibility must be maintained.
    */

    public class CSVReaderWriter
    {
        private readonly IColumnHelper _columnHelper;
        private StreamReader _readerStream;
        private StreamWriter _writerStream;
        private readonly char[] _separator = { '\t' };

        public enum Mode { Read = 1, Write = 2 };

        public CSVReaderWriter()
        {
            _columnHelper = new ColumnHelper(_separator);
        }

        public CSVReaderWriter(IColumnHelper columnHelper)
        {
            _columnHelper = columnHelper;
        }

        public void Open(string fileName, Mode mode)
        {
            if (!File.Exists(fileName))
            {
                throw new Exception(string.Format("The file '{0}' doesn't exist", fileName));
            }

            switch (mode)
            {
                case Mode.Read:
                    _readerStream = File.OpenText(fileName);
                    break;
                case Mode.Write:
                    var fileInfo = new FileInfo(fileName);
                    _writerStream = fileInfo.CreateText();
                    break;
                default:
                    throw new Exception("Unknown file mode for " + fileName);
            }
        }

        public void Write(params string[] columns)
        {
            if (_writerStream == null)
            {
                throw new Exception("A file in Write mode must be opened before writing");
            }

            var line = _columnHelper.BuildLineFromColumns(columns);

            _writerStream.WriteLine(line);
        }

        public bool Read(string column1, string column2)
        {
            return Read(out column1, out column2);
        }

        public bool Read(out string column1, out string column2)
        {
            if (_readerStream == null)
            {
                throw new Exception("A file in Read mode must be opened before reading");
            }

            var line = _readerStream.ReadLine();
            var lineResult = _columnHelper.ReadCsvLine(line);

            column1 = lineResult.Column1;
            column2 = lineResult.Column2;

            return lineResult.HasValues;
        }

        public void Close()
        {
            _writerStream?.Close();
            _readerStream?.Close();
        }
    }
}
