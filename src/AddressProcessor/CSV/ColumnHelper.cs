﻿using System;
using System.Text;
using AddressProcessing.Model;

namespace AddressProcessing.CSV
{
    public class ColumnHelper : IColumnHelper
    {
        private readonly char[] _separator;

        public ColumnHelper(char [] separator)
        {
            _separator = separator;
        }

        public string BuildLineFromColumns(params string[] columns)
        {
            var stringBuilder = new StringBuilder();

            for (var i = 0; i < columns.Length; i++)
            {
                stringBuilder.Append(columns[i]);
                if ((columns.Length - 1) != i)
                {
                    stringBuilder.Append(_separator);
                }
            }

            return stringBuilder.ToString();
        }

        public LineResult ReadCsvLine(string csvLine)
        {
            var lineResult = new LineResult();

            if (csvLine == null)
            {
                return lineResult;
            }

            var columns = csvLine.Split(_separator);

            if (columns.Length == 0)
            {
                return lineResult;
            }

            lineResult.Column1 = columns[0];
            lineResult.Column2 = columns.Length > 1 ? columns[1] : null;
            lineResult.HasValues = true;

            return lineResult;
        }
    }
}
