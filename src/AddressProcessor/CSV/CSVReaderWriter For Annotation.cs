﻿using System;
using System.IO;

namespace AddressProcessing.CSV
{
    /*
        List three to five key concerns with this implementation that you would discuss with the junior developer. 

        Please leave the rest of this file as it is so we can discuss your concerns during the next stage of the interview process.
        
        1) [Flags] on the enum. Why? Seems that the intent could be to allow to assign both read and write at the same time but 
            a) the code suggests that the operations are axclusives (if ... else if) and 
            b) it's not possible to open a file in read and write at the same time, it throws an exception.
            As a side note, the way it is implemented, wouldn't allow the enum to match at the same time read and write, it should be done with the check with the appropriate bitwise operators (it would throw the "Unknown file mode for" exception if both Read and Write are assigned to the input parameter)

        2) The declaration of the enum inside the class and exposed as public input parameter in the Open method makes difficult the extraction of an interface.
           Would be better to declare it outside, so many classes can use it as a dependency without depending on the CSVReaderWriterForAnnotation class.
        
        3) The first read method is poorly implemented (wrong!?) because it throws an exception while trying to read a new line after the last line in the file. (the loop "while true" will encounter an exception instead of terminating the loop as expected, no null check on the read line before the split)
           Plus is useless because the params are passed by value so the actual value read form the file is not copied and returned to the caller.
           Arguable the decision to return false if it encounters a line without values.

        4) About the design: the flow of the operations is slightly confusing. If the user of the class doesn't call Open before performing the desired operation, the read or the write methods will throw an exception.
           Would be better to enforce the workflow (Open -> read/write -> close) using dedicated classes for reading or writing (following the Single Responsability Principle) and maybe open the stream in the constructor,
           avoiding mistakes in opening a file in READ mode and attempt to write for instance, and making the class disposable, suitable for use inside a 'using' block, which would take care of closing the streams.
        
        5) Pointless wrapper methods around WriteLine/ReadLine

    */

    public class CSVReaderWriterForAnnotation
    {
        private StreamReader _readerStream = null;
        private StreamWriter _writerStream = null;

        [Flags]
        public enum Mode { Read = 1, Write = 2 };

        public void Open(string fileName, Mode mode)
        {
            if (mode == Mode.Read)
            {
                _readerStream = File.OpenText(fileName);
            }
            else if (mode == Mode.Write)
            {
                FileInfo fileInfo = new FileInfo(fileName);
                _writerStream = fileInfo.CreateText();
            }
            else
            {
                throw new Exception("Unknown file mode for " + fileName);
            }
        }

        public void Write(params string[] columns)
        {
            string outPut = "";

            for (int i = 0; i < columns.Length; i++)
            {
                outPut += columns[i];
                if ((columns.Length - 1) != i)
                {
                    outPut += "\t";
                }
            }

            WriteLine(outPut);
        }

        public bool Read(string column1, string column2)
        {
            const int FIRST_COLUMN = 0;
            const int SECOND_COLUMN = 1;

            string line;
            string[] columns;

            char[] separator = { '\t' };

            line = ReadLine();
            columns = line.Split(separator);

            if (columns.Length == 0)
            {
                column1 = null;
                column2 = null;

                return false;
            }
            else
            {
                column1 = columns[FIRST_COLUMN];
                column2 = columns[SECOND_COLUMN];

                return true;
            }
        }

        public bool Read(out string column1, out string column2)
        {
            const int FIRST_COLUMN = 0;
            const int SECOND_COLUMN = 1;

            string line;
            string[] columns;

            char[] separator = { '\t' };

            line = ReadLine();

            if (line == null)
            {
                column1 = null;
                column2 = null;

                return false;
            }

            columns = line.Split(separator);

            if (columns.Length == 0)
            {
                column1 = null;
                column2 = null;

                return false;
            } 
            else
            {
                column1 = columns[FIRST_COLUMN];
                column2 = columns[SECOND_COLUMN];

                return true;
            }
        }

        private void WriteLine(string line)
        {
            _writerStream.WriteLine(line);
        }

        private string ReadLine()
        {
            return _readerStream.ReadLine();
        }

        public void Close()
        {
            if (_writerStream != null)
            {
                _writerStream.Close();
            }

            if (_readerStream != null)
            {
                _readerStream.Close();
            }
        }
    }
}
