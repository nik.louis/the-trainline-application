﻿using System;
namespace AddressProcessing.Model
{
    public class LineResult
    {
        public bool HasValues { get; set; }
        public string Column1 { get; set; }
        public string Column2 { get; set; }
    }
}
